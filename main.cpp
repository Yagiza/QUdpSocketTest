#include <QDebug>
#include <QCoreApplication>
#include "serverthread.h"
#include "clientthread.h"

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	ServerThread *server = new ServerThread(1000);
	ClientThread *client = new ClientThread(1000);

	server->moveToThread(server);
	server->start();
	client->moveToThread(client);
	client->start();

	server->wait();
	client->wait();

	qDebug() << "All done!";

//	return a.exec();
	return 0;
}
