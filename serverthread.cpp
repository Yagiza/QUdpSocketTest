#include <QDebug>
#include <QUdpSocket>
#include <QTimerEvent>
#include "serverthread.h"

ServerThread::ServerThread(int timeout):
	timeout(timeout)
{}

void ServerThread::run()
{
	qDebug() << "ServerThread::run()";
	QUdpSocket *socket = new QUdpSocket();
	connect(socket, SIGNAL(readyRead()), SLOT(onReadyRead()));
	socket->bind(SERVER_PORT);
	timerId = startTimer(timeout);

	qDebug() << "ServerThread::exec()...";
	exec();
	qDebug() << "ServerThread::exec() finished!";
}

void ServerThread::onReadyRead()
{
	qDebug() << "ServerThread::onReadyRead()";
	QUdpSocket *socket = qobject_cast<QUdpSocket *>(sender());
	qint64 size = socket->pendingDatagramSize();
	QByteArray data;
	data.resize(size);
	QHostAddress srcAddr;
	quint16 srcPort;
	socket->readDatagram(data.data(), data.size(), &srcAddr, &srcPort);
	qDebug() << "data:" << data;
	if (data == "TEST!")
		socket->writeDatagram("REPLY", srcAddr, srcPort);
}

void ServerThread::timerEvent(QTimerEvent *event)
{
	if (event->timerId() == timerId)
		exit();
}
