#ifndef CLIENTTHREAD_H
#define CLIENTTHREAD_H

#include <QThread>
#include <QUdpSocket>

class ClientThread : public QThread
{
	Q_OBJECT

public:
	ClientThread(int timeout);

protected:
	// QObject interface
	virtual void timerEvent(QTimerEvent *event) override;

	// QThread interface
	virtual void run() override;

protected slots:
	void onReadyRead();

private:
	QUdpSocket *socket;
	int timeout;
	int quitTimerId;
	int sendTimerId;
};

#endif // CLIENTTHREAD_H
