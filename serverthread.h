#ifndef SERVERTHREAD_H
#define SERVERTHREAD_H

#include <QThread>
#include <QWaitCondition>

#define SERVER_PORT 45555

class ServerThread : public QThread
{
	Q_OBJECT

public:
	ServerThread(int timeout);

protected:
	// QObject interface
	virtual void timerEvent(QTimerEvent *event) override;

	// QThread interface
	virtual void run() override;

protected slots:
	void onReadyRead();

private:
	int timeout;
	int timerId;
//	QWaitCondition wait;
};

#endif // SERVERTHREAD_H
