#include <QDebug>
#include <QTimerEvent>
#include "serverthread.h"
#include "clientthread.h"

ClientThread::ClientThread(int timeout):
	socket(NULL),
	timeout(timeout),
	quitTimerId(0),
	sendTimerId(0)
{}

void ClientThread::run()
{
	qDebug() << "ClientThread::run()";
	socket = new QUdpSocket();
	connect(socket, SIGNAL(readyRead()), SLOT(onReadyRead()));
	socket->connectToHost(QHostAddress(QHostAddress::LocalHost), SERVER_PORT);
	quitTimerId = startTimer(timeout);
	sendTimerId = startTimer(100);

	qDebug() << "ClientThread::exec()...";
	exec();
	qDebug() << "ClientThread::exec() finished!";
}

void ClientThread::onReadyRead()
{
	qDebug() << "ClientThread::onReadyRead()";
	qint64 size = socket->pendingDatagramSize();
	QByteArray data;
	data.resize(size);
	QHostAddress srcAddr;
	quint16 srcPort;
	socket->readDatagram(data.data(), data.size(), &srcAddr, &srcPort);
	qDebug() << "data:" << data;
}

void ClientThread::timerEvent(QTimerEvent *event)
{
	if (event->timerId() == quitTimerId)
		exit();
	else if (event->timerId() == sendTimerId) {
		socket->write("TEST!");
	}
}
